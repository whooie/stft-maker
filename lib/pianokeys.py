import numpy as np
import re

def keynum(key):
    if isinstance(key, (int, float)):
        return key
    elif isinstance(key, str):
        pattern = re.compile("([A-G])([#bn]?)([\-]?)([0-9]+)")
        match = pattern.match(key)
        octave_key_nums = {
                "C": 1,
                "D": 3,
                "E": 5,
                "F": 6,
                "G": 8,
                "A": 10,
                "B": 12
            }
        sharp_flat = {
                "#": 1,
                "b": -1,
                "n": 0,
                "": 0
            }
        if match:
            return 12*int(match.group(3)+match.group(4)) \
                    + octave_key_nums[match.group(1)] \
                    + sharp_flat[match.group(2)] \
                    - 9
        else:
            return None
    else:
        return None

def keyname(key, accidental="#", tex=False):
    if isinstance(key, str):
        pattern = re.compile("([A-G])([#bn]?)([\-]?)([0-9]+)")
        match = pattern.match(key)
        return key if match else None
    elif isinstance(key, (int, float)):
        octave_key_names = {
                1: "C",
                3: "D",
                5: "E",
                6: "F",
                8: "G",
                10: "A",
                12: "B"
            }
        if accidental == "#":
            acc = 1
        elif accidental == "b":
            acc = -1
        else:
            return None
        octave_num = (key-1 + 9)//12
        octave_key_num = (key + 9)%12 if (key + 9)%12 != 0 else 12
        for k in octave_key_names.keys():
            if octave_key_num - k == 0:
                if tex:
                    return octave_key_names[octave_key_num] + "$_"+str(octave_num)+"$"
                else:
                    return octave_key_names[octave_key_num] + str(octave_num)
            elif octave_key_num - k == acc:
                if tex:
                    return octave_key_names[octave_key_num-acc]+accidental+"$_"+str(octave_num)+"$"
                else:
                    return octave_key_names[octave_key_num-acc] + accidental + str(octave_num)
        return None
    else:
        return None

def freq(key, a4=440):
    _keynum = keynum(key)
    return a4*2**((_keynum-49)/12)

def find_keyname(freq, a4=440, accidental="#", tex=False):
    _keynum = int(np.round(12*np.log2(freq/a4) + 49))
    return keyname(_keynum, accidental, tex)

