# stft-maker

Tool to compute and animate the discrete-time short-time Fourier transform
(STFT) of a `.wav` audio files. Requires `numpy`, `scipy`, `matplotlib`, and
`ffmpeg-python`, all of which can be found on PyPI and installed via `pip`. Oh
and I guess it requires `ffmpeg` as well; details can be found on [the official
site](https://ffmpeg.org/) or probably your favorite package repository.

```
Usage: stft.py [ options... ] <audio_file.wav>
Options:
    -r, --fps <float>
        Controls the fps of the output video as well as the STFT sample size.
        Default value is 30.
    -f, --fmax <float>
        Set the horizontal-axis limit. Values less than or equal to zero plot
        out to the Nyquist frequency of the STFT at the given fps.
    -a, --amax <float>
        Set the vertical-axis limit. Values less than or equal to zero plot up
        to the highest peak found in any frame.
    -m, --as-mono
        Convert the audio data to mono before processing and plotting as a
        single channel.
    -k, --key-names
        Label peaks with piano key names (e.g. 'A4').
    -s, --peak-sensitivity <float>
        Set the threshold prominence of labeled peaks to be Fmax/sensitivity,
        where Fmax is the maximum value attained globally of the STFT amplitude
        modulus. Default value is 25.
    -o, --output
        Set the final video+audio combined filename.
    -y, --overwrite-output
        Overwrite output files without asking.
    -h, --help
        Display this text and exit.
```
