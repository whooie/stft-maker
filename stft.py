#!/usr/bin/python

# requires: numpy, scipy, matplotlib, ffmpeg-python

import numpy as np
import numpy.fft as fft
import scipy.io.wavfile as wav
import scipy.signal as sig
import matplotlib.pyplot as pp
import matplotlib.animation as anim
import ffmpeg

import getopt
import os.path as path
import sys

import lib.plotdefs as pd
import lib.pianokeys as pk

config = type("Config", (), {
    "fps": 30,
    "fmax": -1,
    "amax": -1,
    "as_mono": False,
    "key_names": False,
    "peak_sensitivity": 25,
    "outfilename": None,
    "overwrite_output": False
})()

helptext = """
A tool to compute and animate the discrete-time short-time Fourier transform
(STFT) of .wav audio files.

Usage: stft.py [ options... ] <audio_file.wav>
Options:
    -r, --fps <float>
        Controls the fps of the output video as well as the STFT sample size.
        Default value is 30.
    -f, --fmax <float>
        Set the horizontal-axis limit. Values less than or equal to zero plot
        out to the Nyquist frequency of the STFT at the given fps.
    -a, --amax <float>
        Set the vertical-axis limit. Values less than or equal to zero plot up
        to the highest peak found in any frame.
    -m, --as-mono
        Convert the audio data to mono before processing and plotting as a
        single channel.
    -k, --key-names
        Label peaks with piano key names (e.g. 'A4').
    -s, --peak-sensitivity <float>
        Set the threshold prominence of labeled peaks to be Fmax/sensitivity,
        where Fmax is the maximum value attained globally of the STFT amplitude
        modulus. Default value is 25.
    -o, --output
        Set the final video+audio combined filename.
    -y, --overwrite-output
        Allow ffmpeg to overwrite output files without asking.
    -h, --help
        Display this text and exit.
"""

def compute_stft_frames(samplerate, data, fps, as_mono=False):
    """
    Compute the discrete-time short-time Fourier transform (STFT) of the data.
    
    Parameters
    ----------
    samplerate : int
        Sample rate of the data.
    data : numpy array
        Slice of the array returned by scipy's `scipy.io.wavfile.read` function.
    framesize : int
        The number of samples in each STFT frame.
    as_mono : bool
        If the data has more than one channel, convert the first two channels to
        mono before computing the FFT. Otherwise, do separate STFTs for the
        first two channels of the data.

    Returns
    -------
    f : numpy 1D array
        Array of frequencies (up to the Nyquist frequency)
    F : numpy 3D array
        Modulus of the complex-valued STFTs of the data. The first axis indexes
        the STFT data, the second indexes the STFT frames, and the third indexes
        the channels
    """
    N = data.shape[0]
    framesize = int(samplerate/fps)
    numframes = int(np.ceil(N/framesize))

    # 1 convert to mono if necessary by doing (left+right)/sqrt(5) (I'm not sure
    #   why we use sqrt(5) instead of a simple average or quadrature sum, but
    #   it's what Audacity uses)
    # 2 pad the end of the data by enough zeros to get up to an even number
    #   of frames
    # 3 reshape the data so that all frames can be FFT'd in a one-liner instead
    #   of frame-by-frame; it's probably faster this way
    if data.ndim > 1:
        if as_mono:
            X = np.append(
                (data[:,0].astype(np.float64)+data[:,1].astype(np.float64))/np.sqrt(5),
                np.zeros(numframes*framesize - N, dtype=np.float64)
            ).reshape(1, numframes, framesize).T
        else:
            X = np.vstack((data[:,:2].astype(np.float64),
                np.zeros((numframes*framesize - N, 2), dtype=np.float64)
            )).T.reshape(2, numframes, framesize).T
    else:
        X = np.append(data[:].astype(np.float64),
            np.zeros(numframes*framesize - N, dtype=np.float64)
        ).reshape(1, numframes, framesize).T

    # if you want to obey Parseval's theorem, multiply F by
    # sqrt(2/samplerate)
    F = np.abs(fft.fft(X, norm="ortho", axis=0)[:framesize//2,:,:])
    f = fft.fftfreq(framesize, 1/samplerate)[:framesize//2] 
    return f, F


def find_peak_keys(f, Fk, prominence=None):
    """
    Return a list of tuples giving the locations, heights, and piano key names
    of peaks found in a single STFT frame.

    Parameters
    ----------
    f : numpy 1D array
        Array of frequencies.
    Fk : numpy 1D array
        Modulus of a single STFT frame.

    Returns
    -------
    peaks : list[tuple[float, float, str]]
        List of (x, y, s) peak tuples where `x` is the frequency of the peak,
        `y` is the peak's height, and `s` is a string giving the piano key name
        of the frequency.
    """
    p = prominence if prominence is not None else (Fk.max()-Fk.min())/5
    peaks = sig.find_peaks(Fk, prominence=p)[0]
    return [(f[i], Fk[i], pk.find_keyname(f[i], accidental="#", tex=True)) for i in peaks]

def _anim_func(i, f, F, ax, lines, texts, key_names, peak_sensitivity, p=True):
    assert F.shape[2] == len(lines) == len(texts)
    if p:
        print(("  {:"+str(len(str(F.shape[1])))+".0f}/{:.0f}  ({:5.1f}%)\r")
            .format(i+1, F.shape[1], 100*(i+1)/F.shape[1]), end="", flush=True)
    # loop over channels
    for j in range(len(lines)):
        # plot lines
        lines[j].set_ydata((1-2*j)*F[:,i,j])

        # can't just del the text objects, have to call their .remove() methods
        while len(texts[j]) > 0:
            texts[j].pop(0).remove()
        if key_names:
            peaks = find_peak_keys(f, F[:,i,j], F.max()/peak_sensitivity)
            ylim = ax.get_ylim()[1]
            for peak in peaks:
                texts[j].append(ax.text(peak[0], (1-2*j)*min(peak[1]+0.035*ylim, 0.95*ylim), peak[2],
                    verticalalignment="center", horizontalalignment="center",
                    fontsize="x-small"))
    # for blit=True, this return doesn't mean anything otherwise
    return lines

def do_animation(f, F, fps, fmax, amax, key_names, peak_sensitivity, filename,
        p=True):
    """
    Generate a video file animating the plots of the STFT frames found in `F`.

    Parameters
    ----------
    f : numpy 1D array
        Array of frequencies.
    F : numpy 3D array
        Modulus of the complex-valued STFTs of the data as output by
        `compute_stft_frames`.
    fps : float
        Frames-per-second rate of the animated output.
    fmax : float
        Maximum frequency included in each plot.
    amax : float
        Maximum amplitude modulus value included in each plot.
    key_names : bool
        Label peaks with corresponding piano key names.
    peak_sensitivity : float
        Threshold prominence of labeled peaks as Fmax/sensitivity, where Fmax is
        the maximum value of `F`.
    p : bool
        Print progress when animating.

    Returns
    -------
    None
    """
    # init the plot
    fig, ax = pp.subplots()

    _fmax = fmax if fmax > 0 else f.max()
    _amax = amax if amax > 0 else F.max()
    ax.set_xlim(-_fmax*0.025, _fmax*1.025)
    if F.shape[2] == 1:
        ax.set_ylim(-0.1*_amax, 1.1*_amax)
    else:
        ax.set_ylim(-1.05*_amax, 1.05*_amax)
    ax.set_yticklabels(list()) # the exact values of the amplitude modulus aren't important
    ax.set_xlabel("Frequency [Hz]")
    ax.set_ylabel("Amplitude Modulus")
    pd.grid(True, ax)

    lines = list()
    texts = list()
    # reverse the creation order to get the zorder right
    for j in reversed(range(F.shape[2])):
        lines.insert(0, ax.plot(f, f.shape[0]*[np.nan], f"C{j:.0f}")[0])
        texts.append(list())

    numframes = F.shape[1]
    frames = list(range(numframes))
    ani = anim.FuncAnimation(fig,
        _anim_func, fargs=(f, F, ax, lines, texts, key_names, peak_sensitivity, p),
        frames=frames, interval=1000/fps, repeat=False)
    ani.save(filename)
    if p:
        print("\n", end="")
    return None

def stft(infilename, outfilename=None, fps=30, fmax=-1, amax=-1, as_mono=False,
        key_names=False, peak_sensitivity=25, overwrite_output=False, p=False):
    """
    Main operation of the script in regular python functional form.

    Parameters
    ----------
    infilename : str
        Input .wav file name.
    outfilename : str (optional)
        Output video file name.
    fps : float (optional)
        Frames-per-second rate of the animated output. Default is 30.
    fmax : float (optional)
        Maximum frequency included in each plot. Values less than or equal to
        zero plot out to the Nyquist frequency of the STFT at the given fps.
    amax : float (optional)
        Maximum amplitude modulus value included in each plot. Values less than
        or equal to zero plot up to the maximum value attained globally over
        the STFT.
    key_names : bool (optional)
        Label peaks with corresponding piano key names.
    peak_sensitivity : float (optional)
        Threshold prominence of labeled peaks as Fmax/sensitivity, where Fmax is
        the maximum value of the STFT.
    overwrite_output : bool (optional)
        Allow ffmpeg to overwrite the output file without asking.
    p : bool
        Print progress when animating.

    Returns
    -------
    None
    """
    video_filename = path.splitext(infilename)[0]+"_video.mkv"
    _outfilename = path.splitext(infilename)[0]+".mkv" \
            if outfilename is None else outfilename
    
    samplerate, data = wav.read(infilename)
    f, F = compute_stft_frames(samplerate, data, fps, as_mono)
    do_animation(f, F, fps, fmax, amax, key_names, peak_sensitivity, video_filename, p)
    
    video = ffmpeg.input(video_filename)
    audio = ffmpeg.input(infilename)
    outstream = ffmpeg.output(video, audio, _outfilename, c="copy")
    if overwrite_output:
        outstream = outstream.overwrite_output()
    outstream.run()


def main(argv):
    shortopts = "r:f:a:mks:o:yh"
    longopts = [
        "fps=",
        "fmax=",
        "amax=",
        "as-mono",
        "key-names",
        "peak-sensitivity=",
        "output=",
        "overwrite-output",
        "help"
    ]
    try:
        opts, args = getopt.gnu_getopt(argv[1:], shortopts, longopts)
        for opt, optarg in opts:
            if opt in ["-r", "--fps"]:
                config.fps = float(optarg)
            elif opt in ["-f", "--fmax"]:
                config.fmax = float(optarg)
            elif opt in ["-a", "--amax"]:
                config.amax = float(optarg)
            elif opt in ["-m", "--as-mono"]:
                config.as_mono = True
            elif opt in ["-k", "--key-names"]:
                config.key_names = True
            elif opt in ["-s", "--peak-sensitivity"]:
                config.peak_sensitivity = float(optarg)
            elif opt in ["-o", "--output"]:
                config.outfilename = optarg
            elif opt in ["-y", "--overwrite-output"]:
                config.overwrite_output = True
            elif opt in ["-h", "--help"]:
                print(helptext)
                return 0
        infilename = args[0]
    except Exception as e:
        raise e
    
    stft(infilename, config.outfilename, config.fps, config.fmax, config.amax,
        config.as_mono, config.key_names, config.peak_sensitivity,
        config.overwrite_output, p=True)

if __name__ == "__main__":
    sys.exit(main(sys.argv))

